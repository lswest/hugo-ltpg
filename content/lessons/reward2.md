---
title: "Your first Go Reward"
date: 2019-03-31T10:05:17+02:00
draft: false
layout: info
returnTo: "index.html#techniques"
---

# | Climbing the mountain
## One step at a time...

> The board is a mirror of the mind of the players as the moments pass. When a master studies the record of a game he can tell at what point greed overtook the pupil, when he became tired, when he fell into stupidity, and when the maid came by with tea.

**Still here, huh. Did you ask yourself why?**

I wondered once myself. Still have no real idea :D. Is it fun? Sometimes. Other times it is painful. Constantly reminds us about our blatant imperfection. Certainly a humbling experience. But the people are kind. Friends in suffering maybe. And the top is not even in sight.

And now that computers have shown us that there is a level of play even beyond what we imagined? You can safely assume that the top will be hidden in clouds forever. And yet we keep climbing.

![Rage](/images/samurai.jpg)

I am happy you joined us. And since you are doing so good, I have unlocked one very special technique for you. 
---
title: "Your first Go Reward"
date: 2019-03-31T10:05:17+02:00
draft: false
layout: "info"
returnTo: "index.html#techniques"
---

# | Being a Go Player
## What your first actual game will be like...

> After losing his most prized title, the 25th Meijin, to Yoda by 4-0 Cho Chikun (one of the best Japanese Go players) was interviewed by a reporter who asked, "Why do you like Go so much?"
He replied: "I hate Go".

If you want to play Go, you have to first be ready to lose. Chances are you will lose your first game, chances are you will lose the second, and the third... Might be 50 games before you also start winning on some regular basis. Right now, you have no idea what abyss there is between you and barely mediocre players. The learning curve is LONG. But you WILL start winning games, you WILL start climbing in rank, the day will come.

And when it does, when you finally start getting noticeably better you will still lose almost 50% of your games. And it will stay like that forever. No matter what you learn, no matter how much your reading improves, there will always be players even better than you. Always. Forever. That is why we hate this game. But it is also why we love it. To death. If you can't take that, leave while you still can. If you can live with that, then welcome among us. Come over to OGS and (if he is still there) tell AdamR that you finished his guide. Maybe even challenge me to your first game.

My advice to you is not to make winning the ultimate goal. Make the goal to improve instead (yes, there is a difference) or even better to have fun. When presented with a choice between a safe winning move and not so safe, but fun looking one, remember this guide and choose the fun-move. As a thank you to me for taking the time to write this. Stop caring about the wins and they will come on their own.

And to set the mood, here is a short animation I made ages ago. Enjoy. And when you are done watching go play some games. And after you have played some, try some of the advanced topics.

{{< youtube quqTe1KfmHQ >}}

{{< alert "mt-8" >}} 
And if you want to get some perspective about how hard the go journey will be, check out this awesome {{< externalLink target="http://home.earthlink.net/~inkwolf/aji/aji1.html" name="comic" >}}
{{< /alert >}} 	
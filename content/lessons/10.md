---
title: "Nets"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "10"
puzzles:
- id: pzl1
  target: 10-1
  text: Puzzle 1
- id: pzl2
  target: 10-2
  text: Puzzle 2
returnTo: "index.html#techniques"
---

# | Nets
## First ladders, now nets. We have a lot of weird metaphors

> Sometimes (or rather often) capturing something means not attacking directly, but surrounding a bit from a distance.  

**It is very simple, (again?)... If you place your stones too close to your opponents, they may become a target. And while you are busy making sure your stones are safe, your opponent gets away.**

However, if you play just the right distance apart, your opponent will still not have enough space to make life, but you will have enough time to safely connect all your stones. Sounds simple? Test it out! 

{{< tsumego >}}

That's all there is to it. Netting something just means surrounding from a distance and waiting patiently. Go is all about balance. Do not ask for too much nor too little. Do not attack above your strengh, and yet, do not let your opponent get away with too much. Just always try for a tiny little bit more than your opponent is getting. 
---
title: "Placing the Stones"
date: 2019-03-31T10:07:35+02:00
draft: false
cookieSetting: "1"
returnTo: "index.html#rules"
---

# | Placing the Stones
## Hard to master, but easy to learn

> Go is a wonderfull ancient game and still the most popular boardgame in the world. And while it is infamously hard to master all the intricate possible strategies, learning the rules should not take more than a few minutes. And you do not need to be a professional to fully enjoy a match.

Ordinarily, Go is played on a square grid of 19x19 or 9x9 lines With a set of black stones for one player and white stones for the other. The goal of the game is to surround more points than your opponent (but more about that later).

{{< jgoboard >}}

{{< rule >}}
The stones are played on the {{< black "intersections" >}}, not inside the "squares".
{{< /rule >}}

{{< rule >}}
{{< black "Black" >}} plays first.
{{< /rule >}}

{{< rule >}}
After that black and white take turns {{< black "alternately">}}.
{{< /rule >}}

{{< rule >}}
Once a stone is played it remains stationary, unless {{< black "captured">}}.
{{< /rule >}}

**Feel free to test out a few moves (if you have not already) for both black and white, you are now already almost ready to play! We will talk about capturing in the next chapter.
And if you really want to know everything, check out the extra information below.**


{{< extra "1" "Compensating the first move" >}}
	Is it not unfair, that black gets the first move? It sure is! To make up for this advantage, white gets several points extra from the start (called komi). 
    It is usually 6,5 points on the 19x19 board and only 5,5 on 9x9. The half points is to prevent ties.
{{< /extra >}}

{{< extra "2" "A polite first move" >}}
	It is traditional to play one's first move into the upper right corner of the board. This way a player symbolically bows to his opponent (White is traditionally the stronger player). This habit is still common even in online play.
{{< /extra >}}
---
title: "Double Atari"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "8"
puzzles:
- id: pzl1
  target: 8-1
  text: Puzzle 1
- id: pzl2
  target: 8-2
  text: Puzzle 2
- id: pzl3
  target: 8-3
  text: Puzzle 2
returnTo: "index.html#techniques"
---

# | Double Atari
## One atari is nice, but two ataris at once? Now that's something

> I promised you some tips on capturing things. Well this is one of them. :)

**The idea is very simple. If you atari a stone, your opponent (unless he's blind) can usually save it. However, if you atari two different stones with one move, well then your opponent can save only one and you get to eat the other.**

{{< tsumego >}}

This was an easy one, but good to keep in mind whether attacking or defending. I always caution beginners about three diagonal moves (double hane).

![double hane diagram](/images/doublehane.jpg)

It has a tendency to get cut apart when not protected. However, DO NOT disregard that move completely. Once you learn to read out the cuts it can be a very powerful move when applied correctly. Only do it if you can imagine what will happen when your opponent cuts :). 
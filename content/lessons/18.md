---
title: "Where to Play Go"
date: 2019-03-31T10:05:17+02:00
draft: false
layout: info
returnTo: "index.html#what_now"
---

# | Where Can I Play?
## Makes sense that you wanna use your skills somewhere

> Learn by doing.  

**These are to my knowledge some of the most popular go servers in the west**

{{< rule >}}
	<a href="https://online-go.com/" target="_blank">www.online-go.com</a>  
{{< /rule >}}

It is no secret that I am an OGS guy. So an argument can be made that I am not objective, but to my knowledge it is the only server where you do not have to download anything, supports both live and correspondence games, and has a great community happy to help beginners with teaching games and reviews if they ask. 

{{< rule >}}
	<a href="http://www.tygem.com/" target="_blank">www.tygem.com</a>  
{{< /rule >}}

{{< rule >}}
	<a href="http://www.wbaduk.com/" target="_blank">www.wbaduk.com</a>  
{{< /rule >}}

{{< rule >}}
	<a href="https://pandanet-igs.com/communities/pandanet" target="_blank">www.pandanet-igs.com</a>  
{{< /rule >}}

{{< rule >}}
	<a href="http://www.gokgs.com/" target="_blank">www.gokgs.com</a>  
{{< /rule >}}

Have fun plying!
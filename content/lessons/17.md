---
title: "Commented Professional Game of Go"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "17"
puzzles:
- id: pzl1
  target: 17-1
  text: A professional game
returnTo: "index.html#strategies"
---

# | Understand a Pro Game
## Can you even?

> For most of us mere humans, a professional strength is something quite unobtainable. But that does not mean we cannot learn from them!    

**First thing to realize before observing a professional game is that you will not understand everything. But you are not even supposed to.**

 Some choices and moves are simply based on a reading ability way beyond ours. If you cannot see the unforseen consequence behind a move, you cannot fully understand it. That's okay. What you should be able to understand however, and what you should strive to realize and incorporate into your games is the discipline. Especially Japanese pros will often play "simple" moves. They will have balance. They attack a bit and then they will defend their own weakness. Where we would play a complicated fancy shape to "do more then just protect" pros will choose a simple solid connection. Calm solid moves. A pressure move then a defensive move. An attack, a defense. Like a wavy ocean. Go forward, go back. Simple, beautifull, calm.

Let's see how much we can understand. I am sure my analysis is not perfect, but neither will be yours. We are amateurs. Do not look for answers, look for inspiration. **Look how the pros are NOT greedy at all**. You will never see a move saying THIS IS ALL MINE! Professionals know that the opponent is entitled to points too.

{{< sgf >}}
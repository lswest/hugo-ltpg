---
title: "Ko"
date: 2019-03-31T10:05:17+02:00
draft: false
cookieSetting: "4"
returnTo: "index.html#rules"
puzzles:
- id: pzl1
  target: 4-1
  text: Puzzle 1
- id: pzl2
  target: 4-2
  text: Puzzle 2  
---

# | No Repetition ("ko" rule)
## No repetition ever

> Full disclosure - this is a rule that may confuse you at first. But it is not as complicated as it may first seem. I for one (although many would disagree) think that a good ko fight is one of the most fun things you can have in a Go game.

**Go games can take a long time. But must not ever be endless. To prevent the possibility of an endless game we have a rule, called ko.**

Just imagine a situation like this one. Given enough stubborness, those players would be playing until they pass out. (feel free to watch for a while)

![ko diagram](/images/ko.gif)

To prevent health problems connected with total fatigue we have this rule:

{{< rule >}}
    A completely identical board position {{< black "must not">}} appear twice in one game.
{{< /rule >}}

{{< rule >}}
Therefore, before you can recapture a stone that just captured yours, you have to {{< black "play elsewhere">}} at least {{< black "once">}}.
{{< /rule >}}

What are your options then? Basically you have two:

1. You deem the stone not important and play elsewhere, letting your opponent end the ko by connecting his endangered stone if he wants to.
2. You deem the stone important enough to fight the ko. You still have to play elsewhere (of course) but you try to find a move your opponent will feel compelled to respond to locally (we call that a ko-threat).



*Confusing? Then check out the following example, it will be clear in no time.*

{{< tsumego >}}

{{< alert >}}
    The hard part - of course - is judging if the threat is big enough to respond to, or whether it is more beneficial to end the ko. But hey, I said the rules are easy, not the game itself.
{{< /alert >}}